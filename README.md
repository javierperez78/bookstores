# Book Stores

This app presents the data from an API, showing the best selling books for each book store, following this wireframe:

![Screenshot](wireframe.png)

## Main challenges

1. Don't use CSS frameworks
1. Responsive design
1. Fetching API (possible CORS problems)
1. Parsing JSON:API, using a library or manually?
1. Interactive rating stars
1. Fetching flag images from API

## Conclusions

1. Let's use Styled Components for CSS
   1. It is popular and easy to implement in React
   1. It allows to use plain CSS
1. Responsive design:
   1. Breakpoints at 500, 768 and 1500px
1. Fetching data from API
   1. No cors problems, the server provides access: `Access-Control-Allow-Origin: *`
   1. `axios` is not needed, we can use `fetch()`
1. Parsing JSON:API
   1. This library was founded after a quick research: [jsonapi-react](https://github.com/aribouius/jsonapi-react)
   1. It seems to parse the jsonapi responses in the best way
   1. We can try this library, and if it doesn't work then going to implement our own jsonapi parser
1. Rating book stores
   1. No persistance (out of scope)
1. Fetching flag images
   1. `restcountries.eu` redirects to `countrylayer.com`, but the free API key doesn't return the flag image
   1. I found another one ([restcountries.com](https://restcountries.com/)), but it is down most of the time
   1. I decided to implement a kind of API call simulation for this, returning the image URL from a CDN
1. No pages/routing is needed, everything is shown in only one page


## Planning / tasks

1. Create a basic ReactJS application
1. Design and create the responsive template
1. Create React components
1. Implement the data fetchers/parsers (stores and flags APIs)

## How to run it

- server: `yarn run api`
- client: `yarn run start`