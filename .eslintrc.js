const RULES = {
    OFF: 'off',
    WARN: 'warn',
    ERROR: 'error',
}

module.exports = {
    env: {
        browser: true,
        es2021: true,
        node: true,
    },
    overrides: [
        {
            files: ['**/*.test.js'],
            env: {
                jest: true,
            },
            plugins: ['jest'],
            rules: {},
        },
    ],
    extends: ['plugin:react/recommended', 'standard', 'prettier'],
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
        },
        ecmaVersion: 12,
        sourceType: 'module',
    },
    plugins: ['react'],
    rules: {
        'react/prop-types': RULES.OFF,
        'react/react-in-jsx-scope': RULES.OFF,
    },
}
