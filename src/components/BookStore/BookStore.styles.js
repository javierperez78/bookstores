import styled from 'styled-components'
import { MQ2, MQ234, MQ34, MQ4 } from 'styles/theme'

export const StoreContainer = styled.div`
    background: #fff;
    display: grid;
    grid-template-columns: 1em auto auto 0.5fr 1em;
    grid-template-rows: 0 auto auto auto auto;
    grid-gap: 1em;
    grid-template-areas:
        'image image image image image'
        'title title title title title'
        '. link link stars .'
        '. books books books .'
        '. date ... flag .';
    padding: 0 0 1em 0;

    @media ${MQ234} {
        grid-template-columns: auto 2fr 0.5fr;
        grid-template-rows: 2em auto auto;
        grid-gap: 1.5em;
        grid-template-areas:
            'image title stars'
            'image books books'
            'date  link  flag';
        padding: 1.5em;
    }

    @media ${MQ2} {
        border-top: 1px solid #ccc;
    }

    @media ${MQ34} {
        border-radius: 1em;
        border: 1px solid #ccc;
        box-shadow: #aaa 5px 5px 10px;
    }

    @media ${MQ4} {
        grid-template-columns: minmax(150px, auto) 2fr 0.5fr;
    }
`
