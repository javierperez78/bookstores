import BookStoreBooks from 'components/BookStoreBooks'
import BookStoreDate from 'components/BookStoreDate'
import BookStoreFlag from 'components/BookStoreFlag'
import BookStoreImage from 'components/BookStoreImage'
import BookStoreLink from 'components/BookStoreLink'
import BookStoreStars from 'components/BookStoreStars'
import BookStoreTitle from 'components/BookStoreTitle'
import BookStoreContext from 'context/BookStoreContext'
import { StoreContainer } from './BookStore.styles'

export default function BookStore({ store }) {
    return (
        <BookStoreContext.Provider value={store}>
            <StoreContainer>
                <BookStoreImage />
                <BookStoreTitle />
                <BookStoreStars />
                <BookStoreBooks />
                <BookStoreDate />
                <BookStoreLink />
                <BookStoreFlag />
            </StoreContainer>
        </BookStoreContext.Provider>
    )
}
