import BookStoreContext from 'context/BookStoreContext'
import useFlag from 'hooks/useFlag'
import { useContext } from 'react'
import { BookStoreFlagImg, StyledBookStoreFlag } from './BookStoreFlag.styles'

export default function BookStoreFlag() {
    const { countries } = useContext(BookStoreContext)
    const { code } = countries
    const { flag } = useFlag(code)

    return (
        <StyledBookStoreFlag className="store-flag">
            <BookStoreFlagImg src={flag} alt={code} />
        </StyledBookStoreFlag>
    )
}
