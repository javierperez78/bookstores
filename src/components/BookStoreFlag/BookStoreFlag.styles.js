import styled from 'styled-components'

export const StyledBookStoreFlag = styled.div`
    grid-area: flag;
    margin-left: auto;
`

export const BookStoreFlagImg = styled.img`
    height: 1em;
`
