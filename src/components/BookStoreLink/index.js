import BookStoreContext from 'context/BookStoreContext'
import { useContext } from 'react'
import { StyledBookStoreLink } from './BookStoreLink.styles'

export default function BookStoreLink() {
    const { website } = useContext(BookStoreContext)
    const { hostname } = new URL(website)
    const cleanHostName = hostname.match(/^(www\.)?(.*)$/)[2]
    return (
        <StyledBookStoreLink>
            <a href={website} target="_blank" rel="noreferrer">
                {cleanHostName}
            </a>
        </StyledBookStoreLink>
    )
}
