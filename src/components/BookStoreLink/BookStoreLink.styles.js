import styled from 'styled-components'

export const StyledBookStoreLink = styled.div`
    grid-area: link;
    font-size: 0.75em;
`
