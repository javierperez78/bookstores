import styled from 'styled-components'
import { MQ1 } from 'styles/theme'

export const StyledBookStoreDate = styled.div`
    color: #666;
    font-size: 0.75em;
    grid-area: date;
    margin: 0 auto;

    @media ${MQ1} {
        margin: 0;
    }
`

export const DateContainer = styled.span`
    display: inline-block;
    padding-left: 1.25em;
    background: url(https://systemuicons.com/images/icons/calendar_date.svg) 0
        3px / 1em 1em no-repeat;
`
