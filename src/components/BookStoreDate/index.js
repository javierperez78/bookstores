import BookStoreContext from 'context/BookStoreContext'
import { useContext } from 'react'
import { DateContainer, StyledBookStoreDate } from './BookStoreDate.styles'

export default function BookStoreDate() {
    const { establishmentDate } = useContext(BookStoreContext)

    const date = new Date(establishmentDate)
    const dateString = new Intl.DateTimeFormat('de-CH').format(date)

    return (
        <StyledBookStoreDate>
            <DateContainer>{dateString}</DateContainer>
        </StyledBookStoreDate>
    )
}
