import Books from './Books'
import { BookStoreBooksList } from './BookStoreBooks.styles'

export default function BookStoreBooks() {
    return (
        <BookStoreBooksList>
            <h3>Best selling books:</h3>
            <Books />
        </BookStoreBooksList>
    )
}
