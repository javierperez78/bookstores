import BookStoreContext from 'context/BookStoreContext'
import { Fragment, useContext } from 'react'
import { BookAuthor, BookTitle, NoBooks, StyledBooks } from './Books.styles'

export default function Books() {
    const { books } = useContext(BookStoreContext)
    return (
        <StyledBooks>
            {books ? (
                books
                    .sort((a, b) => (a.copiesSold < b.copiesSold ? 1 : -1))
                    .slice(0, 3)
                    .map((book) => {
                        return (
                            <Fragment key={book.id}>
                                <BookTitle>{book.name}</BookTitle>
                                <BookAuthor>{book.author.fullName}</BookAuthor>
                            </Fragment>
                        )
                    })
            ) : (
                <NoBooks>no data available</NoBooks>
            )}
        </StyledBooks>
    )
}
