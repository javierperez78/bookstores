import styled from 'styled-components'
import { MQ1, MQ234 } from 'styles/theme'

export const StyledBooks = styled.div`
    display: grid;
    grid-gap: 0.5em;
    padding: 0.5em 0;

    @media ${MQ234} {
        grid-template-columns: 3fr 2fr;
        grid-gap: 0;
    }
`

export const BookTitle = styled.span`
    @media ${MQ234} {
        font-weight: 600;
        font-size: 1em;
        border-bottom: 1px solid #ccc;
        padding: 0.5em 0.5em 0.5em 0;
    }
`

export const BookAuthor = styled.span`
    font-style: italic;
    font-size: 0.75em;

    @media ${MQ1} {
        background: url(https://systemuicons.com/images/icons/user_male_circle.svg)
            0 0 no-repeat;
        padding: 0 0 0.5em 1.75em;
    }

    @media ${MQ234} {
        font-style: italic;
        font-size: 0.75em;
        border-bottom: 1px solid #ccc;
        padding: 0.5em 0 0.5em 0.5em;
        text-align: right;
    }
`

export const NoBooks = styled.span`
    color: #aaa;
    font-size: 0.75em;
`
