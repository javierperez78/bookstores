import styled from 'styled-components'
import { MQ234, MQ4 } from 'styles/theme'

export const BookStoreImageContainer = styled.div`
    grid-area: image;
`

export const BookStoreImageElement = styled.img`
    object-fit: cover;
    width: 100%;
    height: 4em;

    @media ${MQ234} {
        object-fit: cover;
        border-radius: 50%;
        width: 15vw;
        height: 15vw;
    }

    @media ${MQ4} {
        width: 150px;
        height: 150px;
    }
`
