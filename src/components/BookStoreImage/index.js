import BookStoreContext from 'context/BookStoreContext'
import { useContext } from 'react'
import {
    BookStoreImageContainer,
    BookStoreImageElement,
} from './BookStoreImage.styles'

export default function BookStoreImage() {
    const { storeImage, name } = useContext(BookStoreContext)
    return (
        <BookStoreImageContainer>
            <BookStoreImageElement src={storeImage} alt={name} />
        </BookStoreImageContainer>
    )
}
