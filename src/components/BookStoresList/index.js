import BookStore from 'components/BookStore'
import { useStores } from 'hooks/useStores'
import { BookStoresContainer } from './BookStoresList.styles'

export default function BookStoresList() {
    const { stores, isLoading, isFetching } = useStores()

    return (
        <>
            {isLoading || isFetching ? (
                <h2>Loading...</h2>
            ) : (
                <BookStoresContainer>
                    {stores.map((store) => (
                        <BookStore key={store.id} store={store} />
                    ))}
                </BookStoresContainer>
            )}
        </>
    )
}
