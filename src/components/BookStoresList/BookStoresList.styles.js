import styled from 'styled-components'
import { MQ1, MQ2, MQ34, MQ4 } from 'styles/theme'

export const BookStoresContainer = styled.section`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;

    @media ${MQ1} {
    }

    @media ${MQ2} {
        gap: 1em;
        margin: 0;
    }

    @media ${MQ34} {
        gap: 3em;
        margin: 3em;
    }

    @media ${MQ4} {
        flex-direction: row;
        flex-wrap: wrap;
        justify-content: center;
        align-items: flex-start;
    }
`
