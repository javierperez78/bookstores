import { StarContainer } from './BookStoreStars.styles'

const { useState } = require('react')

export default function StarRating({ value }) {
    const [rating, setRating] = useState(parseInt(value) || 0)
    const [selection, setSelection] = useState(rating)

    const handleSelect = (value) => {
        setSelection(value || rating)
    }

    return (
        <div>
            {Array.from({ length: 5 }, (v, i) => (
                <Star
                    value={i + 1}
                    key={i}
                    marked={selection >= i + 1}
                    onSelect={handleSelect}
                    onRating={setRating}
                />
            ))}
        </div>
    )
}

const Star = ({ value, marked, onSelect, onRating }) => {
    const handleMouseOut = (e) => {
        onSelect(null)
    }
    const handleMouseOver = (e) => {
        onSelect(value)
    }
    const handleClick = (e) => {
        onRating(value)
    }

    return (
        <StarContainer
            role="button"
            onMouseOut={handleMouseOut}
            onClick={handleClick}
            onMouseOver={handleMouseOver}
        >
            {marked ? '\u2605' : '\u2606'}
        </StarContainer>
    )
}
