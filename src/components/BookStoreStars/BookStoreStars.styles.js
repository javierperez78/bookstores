import styled from 'styled-components'

export const BookStoreStarsContainer = styled.div`
    grid-area: stars;
    unicode-bidi: bidi-override;
    white-space: nowrap;
    margin-left: auto;
`
export const StarContainer = styled.span`
    color: #333;
    cursor: pointer;
`
