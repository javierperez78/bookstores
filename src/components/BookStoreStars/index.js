import BookStoreContext from 'context/BookStoreContext'
import { useContext } from 'react'
import { BookStoreStarsContainer } from './BookStoreStars.styles'
import StarRating from './StarRating'

export default function BookStoreStars() {
    const { rating } = useContext(BookStoreContext)
    return (
        <BookStoreStarsContainer>
            <StarRating value={rating} />
        </BookStoreStarsContainer>
    )
}
