import BookStoreContext from 'context/BookStoreContext'
import { useContext } from 'react'
import {
    BookStoreTitleContainer,
    BookStoreTitleElement,
} from './BookStoreTitle.styles'

export default function BookStoreTitle() {
    const { name } = useContext(BookStoreContext)
    return (
        <BookStoreTitleContainer>
            <BookStoreTitleElement>{name}</BookStoreTitleElement>
        </BookStoreTitleContainer>
    )
}
