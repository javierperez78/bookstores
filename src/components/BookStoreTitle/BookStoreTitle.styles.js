import styled from 'styled-components'
import { MQ1 } from 'styles/theme'

export const BookStoreTitleContainer = styled.div`
    grid-area: title;
`

export const BookStoreTitleElement = styled.h2`
    font-size: 2em;

    @media ${MQ1} {
        background: #fff;
        display: inline-block;
        opacity: 0.75;
        padding: 0 1em;
    }
`
