const { createContext } = require('react')

const BookStoreContext = createContext(null)
export default BookStoreContext
