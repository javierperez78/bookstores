import BookStoresList from 'components/BookStoresList'
import StoresProvider from 'services/stores/StoresProvider'
import GlobalStyle from 'styles/GlobalStyle'

function App() {
    return (
        <main>
            <GlobalStyle />
            <StoresProvider>
                <BookStoresList />
            </StoresProvider>
        </main>
    )
}

export default App
