import config from 'data/config'
import { ApiClient, ApiProvider } from 'jsonapi-react'

const schema = {
    stores: {
        type: 'stores',
        relationships: {
            countries: {
                type: 'countries',
            },
            books: {
                type: 'books',
            },
        },
    },
    books: {
        type: 'books',
        relationships: {
            author: {
                type: 'authors',
            },
        },
    },
    countries: {
        type: 'countries',
    },
    authors: {
        type: 'authors',
    },
}

export default function StoresProvider({ children }) {
    const client = new ApiClient({
        url: config.DATA_API_BASE_URL,
        schema,
    })

    return <ApiProvider client={client}>{children}</ApiProvider>
}
