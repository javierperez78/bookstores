export default function getFlag(code) {
    const randomValue = Math.floor(Math.random() * 5000)

    // simulating API call
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(`https://flagcdn.com/w320/${code.toLowerCase()}.png`)
        }, randomValue)
    })
}
