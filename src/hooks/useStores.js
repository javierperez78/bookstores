import { useQuery } from 'jsonapi-react'

export const useStores = () => {
    const { data, meta, error, isLoading, isFetching } = useQuery('stores')
    return { stores: data ?? [], meta, error, isLoading, isFetching }
}
