import { useEffect, useState } from 'react'
import getFlag from 'services/flags/getFlag'

export default function useFlag(code) {
    const [flag, setFlag] = useState('/img/flag-placeholder.png')

    useEffect(() => {
        getFlag(code).then(setFlag).catch(console)
    }, [])

    return { flag }
}
