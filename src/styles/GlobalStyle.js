import { createGlobalStyle } from 'styled-components'
import { MQ34 } from './theme'

const GlobalStyle = createGlobalStyle`
    body {
        font-family: "Lucida Sans", "Lucida Sans Regular", "Lucida Grande",
            "Lucida Sans Unicode", Geneva, Verdana, sans-serif;
        font-weight: 300;
        line-height: 1.6;
        font-size: 18px;
    }
    h1,
    h2,
    h3 {
        font-family: "Times New Roman", Times, serif;
    }

    img {
        display: block;
        max-width: 100%;
    }

    a {
        color: #338;
        text-decoration: none;
    }
    a:hover {
        text-decoration: underline;
    }

    @media ${MQ34} {
        body {
            background-color: #e5e5f7;
            background-image: repeating-linear-gradient(45deg, #eee, #ccc);
            background-position: 0 0, 20px 20px;
            background-size: 30px 30px;
        }
    }
`

export default GlobalStyle
