export const BREAKPOINTS = [
    // MQ1
    500,
    // MQ2
    768,
    // MQ3
    1500,
    // MQ4
]

export const MQ1 = `only screen and (max-width: ${BREAKPOINTS[0]}px)`
export const MQ2 = `only screen and (min-width: ${
    BREAKPOINTS[0] + 1
}px) and (max-width: ${BREAKPOINTS[1]}px)`
export const MQ4 = `only screen and (min-width: ${BREAKPOINTS[2] + 1}px)`

export const MQ234 = `only screen and (min-width: ${BREAKPOINTS[0] + 1}px)`
export const MQ34 = `only screen and (min-width: ${BREAKPOINTS[1] + 1}px)`
